<#
.SYNOPSIS
    RH / DSAS staff dir merger
.DESCRIPTION
    The goal of this script is to be able to 
    merge files from two sets of directories
    with eventually slightly different names.
.LINK
    https://gitlab.com/sunoc/documents-merger-for-hr
#> 

[string]$global:all_files_path      = "C:\Users\conusv\Documents\Merge_dossiers_collaborateurs"
[string]$global:DSAS_folder_path    = "C:\Users\conusv\Documents\Merge_dossiers_collaborateurs\DSAS"
[string]$global:08_RH_folder_path   = "C:\Users\conusv\Documents\Merge_dossiers_collaborateurs\08_RH_TFsan"
[string]$global:merged_folder_path  = "C:\Users\conusv\Documents\Merge_dossiers_collaborateurs\Merged"
[string]$global:nomatch_folder_path = "C:\Users\conusv\Documents\Merge_dossiers_collaborateurs\No match"
[int]$global:dir_names_match_percent = 82
[string]$global:timestamp = (Get-Date -Format FileDateTimeUniversal)


function main 
{ 
    <#
    .SYNOPSIS
        Main function
    .DESCRIPTION
        Run the two function used for the merge of the 
        two folders set.
    #>
    [CmdletBinding()]
    param 
    (
        # No input parameters
    )
    process 
    {
        # We gonna check every directories from the DSAS
        foreach($foldersDSAS in Get-ChildItem -Path $DSAS_folder_path)
        {   
            # Used to remove some useless parts of the dir name
            $merged_folder_DSAS = Pure_L1_L2_Remover($foldersDSAS.Name)

            # call the function to find a matching dir
            $matching_RH_dir = Char_Comp_String($merged_folder_DSAS)

            
            
            # if the matching percentage is not reached, move the DSAS dir to a dedicated dir
            if ($matching_RH_dir -eq "no_match") 
            {
                Copy-Item "$DSAS_folder_path\$foldersDSAS" -Destination "$nomatch_folder_path\$merged_folder_DSAS" -Recurse -Force    
            }
            # If we got a matching name, procede to merge
            else 
            {
                Copy-Item -Path "$DSAS_folder_path\$foldersDSAS" -Destination "$merged_folder_path\$merged_folder_DSAS" -Recurse -Force
                Merge_Files "$merged_folder_path\$merged_folder_DSAS" "$08_RH_folder_path\$matching_RH_dir"
            }
        }
    }
}

function Char_Comp_String
{
    <#
    .SYNOPSIS
        Dir names char by char comparaison function
    .DESCRIPTION
        char by char comparaison of strings:
        will return the best matching name of 
        the name of a given dir against all the
        08_RH dirs
    #>
    [CmdletBinding()]
    param 
    (
        [Parameter(Mandatory = $true)][string]$nameDSAS
    )

    $bestmatchcharnum = 0    # biggest number of maching character found
    $bestmatchnameRH  = ""   # name of the best maching dir name
    $shortcharnameRH  = ""   # shorter 08_RH dir name
    $matchingpercent  = 0    # output var: maching percentage
    
    # We gonna check every dir from the 08_RH once again
    foreach($nameRH in Get-ChildItem -Path $08_RH_folder_path)
    {
        # gotta remove the part of 08_RH name left to underscore char
        $shortcharnameRH = Pure_Underscore_Remover($nameRH.Name)


        # maching characters for the currently tested dir
        $matchingcharnum = 0
        
        # counting the matching characters
        for ($i = 0; $i -lt $shortcharnameRH.Length; $i++) {
            if ($nameDSAS[$i] -eq $shortcharnameRH[$i])
            {
                $matchingcharnum = $matchingcharnum +1
            }
        }

        # updating if the current match is the best so far
        if ($matchingcharnum -gt $bestmatchcharnum)
        {
            $bestmatchcharnum = $matchingcharnum

            # here, we save the whole path object
            # not only the shorter name string !!
            $bestmatchnameRH  = $nameRH 
        }

        # calculate the maching percentage of the best match
        if ((Pure_Underscore_Remover($bestmatchnameRH).Length) -eq 0){$matchingpercent = 0}
        else{
            $matchingpercent = ($bestmatchcharnum / (Pure_Underscore_Remover($bestmatchnameRH)).Length)*100
        }

        # if the match is good enough, returns the name for the 08_RH dir
        if ($matchingpercent -gt $dir_names_match_percent)
        {
            return $bestmatchnameRH
        }
    }

    # print out the errors / mismatch infos directly
    Write-Host $nameDSAS "/////" (Pure_Underscore_Remover($bestmatchnameRH)) "/////" $bestmatchcharnum 'char matching, ' $matchingpercent '%'

    # put the result of the mismatchs in a txt file (CSV formatted), for the record
    "$nameDSAS,$bestmatchnameRH,$matchingpercent" | Add-Content -Path "$all_files_path\No match\000_mismatches_$timestamp.txt"
    return "no_match"
    
}

function Pure_Underscore_Remover([string]$dirname)
{
    <#
        Return the part left to the first _ in a given string
        or try to fin a " fin" string, when no _ is found.
        Return the same string if it contains no _ or fin
    #>

    if ($dirname.IndexOf("_") -ne -1)
    {
        # if so, truncate to keep only the part on the left
        $dirname.substring(0, $dirname.IndexOf("_"))
    }
    elseif ($dirname.IndexOf(" fin") -ne -1)
    {
        # else, try to truncae at " fin", when the underscore is missing
        $dirname.substring(0, $dirname.IndexOf(" fin"))
    }
    else
    {
        $dirname
    }
}

function Pure_L1_L2_Remover([string]$dirname)
{
    if ($dirname.IndexOf("L1") -ne -1)
    {
        $dirname.substring($dirname.IndexOf("L1")+3)
    }
    elseif ($dirname.IndexOf("L2") -ne -1)
    {
        $dirname.substring($dirname.IndexOf("L2")+3)
    }
    else
    {
        $dirname
    }
}

function Merge_Files 
{
    <#
    .SYNOPSIS
        Merging function for the maching dir
    .DESCRIPTION
        Merges the files from the matching 08_RH
        dir into the DSAS dir for each staff members
    #>
    [CmdletBinding()]
    param 
    (
        [Parameter(Mandatory = $true)] [string]$pathDSAS,
        [Parameter(Mandatory = $true)] [string]$path08_RH
    )

    process 
    {
        # Get the defaults dir structure from the DSAS
        $01_Docs_contractuels       = "$pathDSAS\01_Docs_contrat"
        $02_Gestion_temps           = "$pathDSAS\02_Gestion_temps"
        $03_Evaluations_avertissmnt = "$pathDSAS\03_Evaluations"
        $04_Dispos_planif           = "$pathDSAS\04_Dispos"
        $05_Divers                  = "$pathDSAS\05_Divers"

        # Gotta check the subdir structure of the DSAS folder
        foreach ($dir in Get-ChildItem -Path $pathDSAS)
        {
            # we are looking for directory names here
            switch -Wildcard ($dir)
            {
                "*01_Doc*" {Rename-Item "$pathDSAS\$dir"  $01_Docs_contractuels}
                "*02_Ges*" {Rename-Item "$pathDSAS\$dir"  $02_Gestion_temps}
                "*03_Eva*" {Rename-Item "$pathDSAS\$dir"  $03_Evaluations_avertissmnt}
                "*04_Dis*" {Rename-Item "$pathDSAS\$dir"  $04_Dispos_planif}
            }
        }

        # Gotta then create the remaining folders that doesn't exist
        if ((Test-Path -Path $01_Docs_contractuels) -ne $true)       { New-Item -Path $01_Docs_contractuels -ItemType Directory | Out-Null}
        if ((Test-Path -Path $02_Gestion_temps) -ne $true)           { New-Item -Path $02_Gestion_temps -ItemType Directory | Out-Null}
        if ((Test-Path -Path $03_Evaluations_avertissmnt) -ne $true) { New-Item -Path $03_Evaluations_avertissmnt -ItemType Directory | Out-Null}
        if ((Test-Path -Path $04_Dispos_planif) -ne $true)           { New-Item -Path $04_Dispos_planif -ItemType Directory | Out-Null}
        if ((Test-Path -Path $05_Divers) -ne $true)                  { New-Item -Path $05_Divers -ItemType Directory | Out-Null}


        # For all the files and folder in the staff dir
        foreach ($item in Get-ChildItem -Path $path08_RH)
        {
            # Check the name and put it in the correct DSAS merge dir
            switch -Wildcard ($item)
            {
                # Contracts / prolongations
                "*contrat*"   {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*prol*"      {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*cv*"        {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*CV*"        {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*urriculum*" {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*ebens*"     {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*certif*"    {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*ENT*"       {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*ntretien*"  {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*tszeugn*"   {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*iplôme*"    {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*iplome*"    {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*otivation*" {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*andidatur*" {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*ttestation*"{Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*identit*"   {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*uestionna*" {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*confidenti*"{Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*CDDH*"      {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*CDDM*"      {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*LF*"        {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*AR*"        {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*démission*" {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}
                "*demission*" {Copy-Item "$path08_RH\$item" -Destination $01_Docs_contractuels -Recurse -Force}

                # Gestion du temp
                "*CM*"        {Copy-Item "$path08_RH\$item" -Destination $02_Gestion_temps -Recurse -Force}
                "*aladie*"    {Copy-Item "$path08_RH\$item" -Destination $02_Gestion_temps -Recurse -Force}
                "*médical*"   {Copy-Item "$path08_RH\$item" -Destination $02_Gestion_temps -Recurse -Force}
                "*medical*"   {Copy-Item "$path08_RH\$item" -Destination $02_Gestion_temps -Recurse -Force}
                "*ccident*"   {Copy-Item "$path08_RH\$item" -Destination $02_Gestion_temps -Recurse -Force}
                "*bsence*"    {Copy-Item "$path08_RH\$item" -Destination $02_Gestion_temps -Recurse -Force}

                # Disponibilité
                "*ispo*"     {Copy-Item "$path08_RH\$item" -Destination $04_Dispos_planif -Recurse -Force}
                "*ignal*"    {Copy-Item "$path08_RH\$item" -Destination $04_Dispos_planif -Recurse -Force}

                # Evaluation / avertissements
                "*valuation*"  {Copy-Item "$path08_RH\$item" -Destination $03_Evaluations_avertissmnt -Recurse -Force}

                # Autres
                Default        {Copy-Item "$path08_RH\$item" -Destination $05_Divers -Recurse -Force}
            }
        }
    }
}

#Entry point
main
