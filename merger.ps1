<#
.SYNOPSIS
    RH / DSAS staff dir merger
.DESCRIPTION
    The goal of this script is to be able to 
    merge files from two sets of directories
    with eventually slightly different names.
.LINK
    https://gitlab.com/sunoc/documents-merger-for-hr
#> 

[string]$global:all_files_path      = "C:\Users\conusv\Documents\Merge_dossiers_collaborateurs"
[string]$global:DSAS_folder_path    = "$all_files_path\DSAS"
[string]$global:08_RH_folder_path   = "$all_files_path\08_RH_TFsan"
[string]$global:merged_folder_path  = "$all_files_path\Merged"
[string]$global:nomatch_folder_path = "$all_files_path\No match"
[int]$global:wrong_char_allowed = 2
[string]$global:timestamp = Get-Date -Format FileDateTimeUniversal

function Check_All_DSAS_Dir([array]$dirarray)
{
    <#
        Entry point. 
        This function will pass on all the DSAS directories
        and apply the merge on each of them.
    #>
    
    # Compare the directory name with all the dir from the 08_RH
    Recursive_Char_Compare -RHarray (Get-ChildItem -Path $08_RH_folder_path -Name) -DSASdir ($dirarray[0])

    # Recursion call, while we have more than 1 element in the array
    if ($dirarray.Count -ne 1) 
    {
        # passes the array, minus the 1st element, back to the function
        Check_All_DSAS_Dir($dirarray[1..($dirarray.Count-1)])
    }
}

function Recursive_Char_Compare([array]$RHarray, [string]$DSASdir)
{    
    <#
        For all the 08_RH dir, try to find a matching name
    #>

    # if we can find a good match, proceed to merge !
    if (Recursive_Match_Finder -refstring (Pure_Underscore_Remover -dirname $RHarray[0]) -comparestring $DSASdir)
    {
        $bestmatchingdir = $RHarray[0]
        Copy-Item -Path "$DSAS_folder_path\$DSASdir" -Destination "$merged_folder_path" -Recurse -Force
        Recursive_Merger -RHfiles (Get-ChildItem -Path "$08_RH_folder_path\$bestmatchingdir" -Name) `
                            -pathDSAS "$merged_folder_path\$DSASdir" `
                            -path08RH "$08_RH_folder_path\$bestmatchingdir"
                            
    }
    else 
    {
        # Keep on searching
        # Recursion call, while we have more than 1 element in the array
        if ($RHarray.Count -ne 1) 
        {
            Recursive_Char_Compare -RHarray $RHarray[1..($RHarray.Count-1)] `
                                    -DSASdir $DSASdir `
        }
        else 
        {
            # No match found amoung the 08_RH dirs
            # Put the name of dirs without a match in a txt file, for checking later
            "$DSASdir" | Add-Content -Path "$all_files_path\mismatch_$timestamp.txt"
            # and move the dir to a dedicated place
            Copy-Item "$DSAS_folder_path\$DSASdir" -Destination "$nomatch_folder_path" -Recurse -Force 
        }
    } 
}

function Pure_Underscore_Remover([string]$dirname)
{
    <#
        Return the part left to the first _ in a given string
        Return the same string if it contains no _
    #>

    if ($dirname.IndexOf("_") -ne -1)
    {
        # if so, truncate to keep only the part on the left
        $dirname.substring(0, $dirname.IndexOf("_"))
    }
    else
    {
        $dirname
    }
}

function Recursive_Match_Finder([string]$refstring, [string]$comparestring, [int]$mismatchings=0)
{
    <#
        Given two strings, return the number of character
        identical (minus the case), and at the same position
    #>
    
    # if the first character is different, increase the counter of mismatch
    if ($refstring[0] -ne $comparestring[0]) { $mismatchings++ }

    # stop to search if the max numbers of mismatchs is exceeded
    if ($mismatchings -gt $wrong_char_allowed) { return $false }

    # Recursion call, while we have more than a 1 char long string
    if ($refstring.Length -gt 1) 
    {
        Recursive_Match_Finder -refstring $refstring.substring(1, ($refstring.Length-1)) `
                                        -comparestring $comparestring.substring(1, ($comparestring.Length-1)) `
                                        -mismatchings $mismatchings
    } 
    else 
    {
        return $true
    }
    
}

function Recursive_Merger([array]$RHfiles, [string]$pathDSAS, [string]$path08RH)
{
    <#
        For all the files found in the matching 08_RH dir,
        put them in the corresponding DSAS dir
    #>
    
    $currentloopfile = $RHfiles[0]
    switch -Wildcard ($currentloopfile)
    {
        # Contracts / prolongations
        "*contrat*"   {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*prol*"      {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*cv*"        {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*CV*"        {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*urriculum*" {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*ebens*"     {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*certif*"    {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*ENT*"       {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*ntretien*"  {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*tszeugn*"   {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*iplôme*"    {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*iplome*"    {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*otivation*" {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*andidatur*" {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*ttestation*"{Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*identit*"   {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*uestionna*" {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*confidenti*"{Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*CDDH*"      {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*CDDM*"      {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*LF*"        {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*AR*"        {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*démission*" {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}
        "*demission*" {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\01_Docs contractuels" -Recurse -Force}

        # Gestion du temp
        "*CM*"        {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\02_Gestion temps, vac, congés, absences" -Recurse -Force}
        "*aladie*"    {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\02_Gestion temps, vac, congés, absences" -Recurse -Force}
        "*médical*"   {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\02_Gestion temps, vac, congés, absences" -Recurse -Force}
        "*medical*"   {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\02_Gestion temps, vac, congés, absences" -Recurse -Force}
        "*ccident*"   {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\02_Gestion temps, vac, congés, absences" -Recurse -Force}
        "*bsence*"    {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\02_Gestion temps, vac, congés, absences" -Recurse -Force}

        # Disponibilité
        "*ispo*"     {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\04_Dispos planif" -Recurse -Force}
        "*ignal*"    {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\04_Dispos planif" -Recurse -Force}

        # Evaluation / avertissements
        "*valuation*"  {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\03_Evaluations, avertissmnt, notes" -Recurse -Force}

        # Autres
        Default        {Copy-Item "$path08RH\$currentloopfile" -Destination "$pathDSAS\05_Divers" -Recurse -Force}
    }

    # Recursion call, while we have more than 1 file to check
    if ($RHfiles.Count -gt 1)
    {
        Recursive_Merger -RHfiles $RHfiles[1..($RHfiles.Count-1)] -pathDSAS $pathDSAS -path08RH $path08RH
    }
}

#Entry point
Check_All_DSAS_Dir -dirarray (Get-ChildItem -Path $DSAS_folder_path -Name)