#+title: Documents merger for HR

The COVID taskforce and the state of Fribourg currently have separated staff folders for the hiring document.
The goal of this repo is to maintain a script that can be used by the HR to merge their files with the state.


* Folders structure
The program will merge the dir from 08_RH_TFsan onto DSAS. The merged dir will then be moved to a Merged dir,
and those that had no matching name will be copied as is in the No match dir.

+ =08_RH_TFsan=
+ =DSAS=
+ =Merged=
+ =No match=

Then every dir under =DSAS= must be in the format
=NAME Firstname= and contain:
+ =01_Docs contractuels=
+ =02_Gestion temps, vac, congés, absences=
+ =03_Evaluations=
+ =04_Dispos planif=
+ =05_Divers=

  
